#![macro_use]

use std::fmt;

pub type AnyError = Box<std::error::Error>;

pub trait IteratorExt: Iterator {
    fn fold_results_vec<A, E>(&mut self) -> Result<Vec<A>, E>
    where
        Self: Iterator<Item = Result<A, E>>,
    {
        let mut ret: Vec<A> = vec![];
        for elt in self {
            match elt {
                Ok(v) => ret.push(v),
                Err(u) => return Err(u),
            }
        }
        Ok(ret)
    }
}

impl<I: Iterator> IteratorExt for I {}

#[allow(unused_macros)]
macro_rules! measure {
    ($code:expr) => {{
        let time = std::time::Instant::now();
        let value = $code;
        println!("Time elapsed: {:?}", time.elapsed());
        value
    }};
    ($code:expr, $msg:expr) => {{
        let time = std::time::Instant::now();
        let value = $code;
        println!("{}: {:?}", $msg, time.elapsed());
        value
    }};
}

#[derive(Debug)]
#[allow(unused)]
pub struct SimpleError {
    pub details: String,
}

impl SimpleError {
    #[allow(unused)]
    pub fn new<M: Into<String>>(msg: M) -> SimpleError {
        SimpleError {
            details: msg.into(),
        }
    }
}

impl fmt::Display for SimpleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl std::error::Error for SimpleError {
    fn description(&self) -> &str {
        &self.details
    }
}
