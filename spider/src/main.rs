#[macro_use]
extern crate lazy_static;

use crate::extensions::AnyError;
use clap::{App, Arg};
use std::fs;
use std::path::Path;

mod extensions;
mod spider;

fn main() -> Result<(), AnyError> {
    let matches = App::new("webint-project")
        .arg(
            Arg::with_name("url")
                .long("url")
                .short("u")
                .value_name("URL")
                .help("Sets the URL the spider should start from")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("pages")
                .long("pages")
                .short("p")
                .value_name("DIR")
                .help("Sets the web page save directory")
                .takes_value(true)
                .default_value("pages"),
        )
        .arg(
            Arg::with_name("scrape")
                .long("scrape")
                .short("s")
                .value_name("DIR")
                .help("Sets the words and links root scrape directory")
                .takes_value(true)
                .default_value("scrapes"),
        )
        .arg(
            Arg::with_name("max")
                .long("max")
                .short("m")
                .value_name("COUNT")
                .help("Sets the max amount of pages that should be scraped")
                .takes_value(true)
                .default_value("200"),
        )
        .arg(
            Arg::with_name("threads")
                .long("threads")
                .short("t")
                .value_name("COUNT")
                .help("Sets the number of threads that are used when downloading pages")
                .takes_value(true)
                .default_value("20"),
        )
        .get_matches();

    let pages_dir = Path::new(matches.value_of("pages").unwrap());
    fs::create_dir_all(pages_dir)?;

    let scrape_dir = Path::new(matches.value_of("scrape").unwrap());
    fs::create_dir_all(scrape_dir)?;

    let url = matches.value_of("url").unwrap();

    let max: usize = matches.value_of("max").unwrap().parse()?;

    let threads: usize = matches.value_of("threads").unwrap().parse()?;

    println!(
        "Scraping {}, going max {} pages, using {} threads, storing pages in {:?}, storing scrapes in {:?}",
        url, max, threads, pages_dir, scrape_dir
    );

    spider::download(url, max, pages_dir, threads)?;
    spider::scrape(pages_dir, scrape_dir)?;

    Ok(())
}
