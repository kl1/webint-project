use crate::extensions::AnyError;
use itertools::Itertools;
use rayon::prelude::*;
use regex::Regex;
use scraper::node::Element;
use scraper::node::Text;
use scraper::{Html, Node};
use std::collections::HashSet;
use std::path::Path;
use std::path::PathBuf;
use std::{cmp, fs, io};
use url::Url;
use walkdir::WalkDir;

pub fn download(
    url: &str,
    max_pages: usize,
    save_dir: impl Into<PathBuf>,
    threads: usize,
) -> Result<(), AnyError> {
    let mut spider = Spider::new(max_pages, save_dir, threads);
    spider.start(url)
}

pub fn scrape(
    input_dir: impl Into<PathBuf>,
    output_dir: impl Into<PathBuf>,
) -> Result<(), AnyError> {
    let input_dir = input_dir.into();
    let output_dir = output_dir.into();

    let files = WalkDir::new(input_dir).into_iter().filter_map(|dir_entry| {
        if let Ok(entry) = dir_entry {
            if entry.file_type().is_file() {
                return Some(entry);
            }
        }
        None
    });

    for file_entry in files {
        let file_name = file_entry.path().file_name().unwrap().to_str().unwrap();
        let url_string = file_name.replace("|", "/");
        println!("scraping: {}", url_string);
        let url = Url::parse(&url_string)?;
        let content = fs::read_to_string(file_entry.path())?;
        let scraped = scrape_outgoing_links_and_words(&url, &content);
        write_scrape(&scraped, file_name, output_dir.as_path())?;
    }

    Ok(())
}

fn write_scrape(scraped: &Scrape, file_name: &str, out_dir: &Path) -> io::Result<()> {
    let link_dir = out_dir.join("links");
    let word_dir = out_dir.join("words");

    fs::create_dir_all(&link_dir)?;
    fs::create_dir_all(&word_dir)?;

    let link_file = link_dir.join(file_name);
    let word_file = word_dir.join(file_name);

    let links = scraped
        .links
        .iter()
        .map(|url| url.as_str())
        .collect_vec()
        .join("\n");
    let words = scraped.texts.join(" ");

    fs::write(&link_file, &links)?;
    fs::write(&word_file, &words)
}

struct Spider {
    queue: Vec<Url>,
    max_count: usize,
    processed_count: usize,
    save_dir: PathBuf,
    thread_pool: rayon::ThreadPool,
    seen: HashSet<String>,
}

impl Spider {
    fn new(max_count: usize, save_dir: impl Into<PathBuf>, threads: usize) -> Spider {
        Spider {
            queue: Vec::new(),
            max_count,
            processed_count: 0,
            save_dir: save_dir.into(),
            thread_pool: rayon::ThreadPoolBuilder::new()
                .num_threads(threads)
                .build()
                .unwrap(),
            seen: HashSet::new(),
        }
    }

    fn start(&mut self, url: &str) -> Result<(), AnyError> {
        self.queue.clear();
        let url = Url::parse(url)?;
        self.queue.push(url);
        self.crawl()
    }

    fn crawl(&mut self) -> Result<(), AnyError> {
        loop {
            for url in &self.queue {
                self.seen.insert(url.as_str().to_string());
            }

            let limit = self.max_count - self.processed_count;

            if limit <= 0 {
                println!("Finished downloading {} pages", self.max_count);
                return Ok(());
            }

            let can_take = cmp::min(limit, self.queue.len());
            let urls = &self.queue[0..can_take];
            self.processed_count += urls.len();

            self.queue = self
                .process_urls(urls)
                .into_iter()
                .filter(|url| !self.seen.contains(url.as_str()))
                .collect();
        }
    }

    fn process_urls(&self, urls: &[Url]) -> Vec<Url> {
        self.thread_pool.install(|| {
            urls.into_par_iter()
                .map(|url| -> Vec<Url> {
                    println!("Downloading: {}", url);

                    let content = match reqwest::get(url.as_str()).and_then(|mut resp| resp.text())
                    {
                        Ok(text) => text,
                        Err(err) => {
                            eprintln!("error downloading url: {}, err: {}", url, err);
                            return vec![];
                        }
                    };

                    if let Err(err) = self.save_page(&url, &content) {
                        eprintln!("error saving page: {}, err: {}", url, err);
                    }

                    all_links(&url, &content)
                })
                .flatten()
                .collect()
        })
    }

    fn save_page(&self, url: &Url, content: &str) -> io::Result<()> {
        let file_name = url.as_str().replace("/", "|"); // can't have / in file names
        let path = self.save_dir.join(file_name);
        std::fs::write(path, content)
    }
}

pub struct Scrape {
    pub links: Vec<Url>,
    pub texts: Vec<String>,
}

pub fn scrape_outgoing_links_and_words(url: &Url, content: &str) -> Scrape {
    let document = Html::parse_document(content);

    let (links, texts) = document.root_element().descendants().fold(
        (Vec::<Url>::new(), Vec::<String>::new()),
        |mut acc, node| {
            match node.value() {
                Node::Text(text) => {
                    for word in parse_words(text) {
                        acc.1.push(word);
                    }
                }
                Node::Element(elem) => {
                    if let Some(link) = parse_link(elem, None) {
                        if link.domain() != url.domain() {
                            acc.0.push(link);
                        }
                    }
                }
                _ => {}
            }
            acc
        },
    );

    Scrape { links, texts }
}

pub fn all_links(base_url: &Url, content: &str) -> Vec<Url> {
    let document = Html::parse_document(content);

    document
        .root_element()
        .descendants()
        .fold(Vec::<Url>::new(), |mut acc, node| {
            match node.value() {
                Node::Element(elem) => {
                    if let Some(link) = parse_link(elem, Some(base_url)) {
                        acc.push(link);
                    }
                }
                _ => {}
            }
            acc
        })
}

lazy_static! {
    static ref NON_WORD_CHARS: Regex = { Regex::new(r"[^\w+]").unwrap() };
}

fn parse_words(text: &Text) -> Vec<String> {
    text.split_whitespace()
        .map(|word| NON_WORD_CHARS.replace_all(word, "").to_lowercase())
        .collect()
}

fn parse_link(elem: &Element, base_url: Option<&Url>) -> Option<Url> {
    if elem.name() == "a" {
        if let Some(link) = elem.attr("href") {
            let parsed = if let Some(base_url) = base_url {
                Url::parse(link).or_else(|_| base_url.join(link))
            } else {
                Url::parse(link)
            };

            match parsed {
                Ok(url) => {
                    if url.scheme() == "http" || url.scheme() == "https" {
                        return Some(url)
                    }
                },
                Err(e) => {
                    if base_url.is_some() {
                        eprintln!(
                            "failed to parse link: {}, with base url: {:?}, error: {:?}",
                            link, base_url, e
                        )
                    }
                }
            }
        }
    }
    None
}
