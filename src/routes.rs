use crate::engine::Engine;
use crate::indexer::Indexer;
use crate::model::parser;
use gotham::helpers::http::response;
use gotham::state::{FromState, State};
use tera::{Context, Tera};

use hyper::{Body, Response, StatusCode};
use itertools::Itertools;
use std::time::Instant;

lazy_static! {
    static ref TERA: Tera = {
        let mut tera: Tera = compile_templates!("templates/**/*");
        tera.autoescape_on(vec![]);
        tera
    };
}

lazy_static! {
    static ref ENGINE: Engine = {
        let mut indexer = Indexer::new();
        let pages = parser::load_pages(&mut indexer).unwrap();
        Engine::new(pages, indexer)
    };
}

/// Creates the engine eagerly so that the first search isn't slowed down
pub fn init_engine() {
    &*ENGINE;
}

// /

pub fn root(state: State) -> (State, Response<Body>) {
    let context = Context::new();
    let html = TERA.render("main.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

// /search

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct SearchExtractor {
    pub query: String,
}

pub fn search(mut state: State) -> (State, Response<Body>) {
    let SearchExtractor { query } = SearchExtractor::take_from(&mut state);

    let time = Instant::now();
    let rankings = ENGINE.query(&query);
    let elapsed = format!("{:?}", time.elapsed());

    let mut context = Context::new();
    context.insert("query", &query);
    context.insert("rankings", &rankings.iter().take(5).collect_vec());
    context.insert("elapsed_time", &elapsed);


    let html = TERA.render("search.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

// Helpers

fn create_html_response<B>(state: &State, body: B) -> Response<Body>
where
    B: Into<Body>,
{
    response::create_response(state, StatusCode::OK, mime::TEXT_HTML, body)
}
