use crate::extensions::*;
use crate::model::entity::Page;
use crate::model::page_rank;

use std::collections::HashSet;
use std::fs;
use std::path::Path;
use walkdir::DirEntry;
use walkdir::WalkDir;

pub trait WordToId {
    fn word_to_id(&mut self, word: &str) -> u32;
}

pub fn load_pages(converter: &mut impl WordToId) -> Result<Vec<Page>, AnyError> {
    let mut pages = scan_files("scrapes/words")?;
    let mut links = scan_files("scrapes/links")?;

    if pages.len() != links.len() {
        return Err(Box::new(SimpleError::new(format!(
            "must have same amount of pages as links. pages: {}, links: {}",
            pages.len(),
            links.len()
        ))));
    }

    // probably not needed but just to be sure we match the correct files in zip()
    pages.sort_by(|a, b| a.path().cmp(b.path()));
    links.sort_by(|a, b| a.path().cmp(b.path()));

    let mut pages = pages
        .into_iter()
        .zip(links.into_iter())
        .map(|(page_file, link_file)| -> Result<Page, AnyError> {
            create_page(page_file.path(), link_file.path(), converter)
        })
        .fold_results_vec()?;

    measure!(page_rank::calculate_page_rank(&mut pages), "page rank");

    Ok(pages)
}

fn scan_files(path: &str) -> Result<Vec<DirEntry>, walkdir::Error> {
    WalkDir::new(path)
        .into_iter()
        .filter(|dir_entry| {
            // filter out directories
            if let Ok(entry) = dir_entry {
                entry.file_type().is_file()
            } else {
                true // handle errors in fold_results_vec()
            }
        })
        .fold_results_vec()
}

fn create_page(
    page_path: &Path,
    links_path: &Path,
    converter: &mut impl WordToId,
) -> Result<Page, AnyError> {
    let content = fs::read_to_string(page_path)?;
    let url = create_url(page_path)?;
    let links = read_links(links_path)?;

    let words = content
        .split_whitespace()
        .filter(|s| *s != "")
        .map(|s| converter.word_to_id(s))
        .collect();

    Ok(Page {
        words,
        url,
        links,
        page_rank: 1.0, // starting value for page rank is 1.0
    })
}

fn create_url(path: &Path) -> Result<String, AnyError> {
    let file_name = path.file_name().ok_or("illegal file name")?.to_string_lossy();
    Ok(file_name.replace("|", "/"))
}

fn read_links(path: &Path) -> Result<HashSet<String>, AnyError> {
    Ok(fs::read_to_string(path)?
        .lines()
        .map(str::to_string)
        .collect())
}
