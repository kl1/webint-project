use crate::model::entity::Page;
use crate::model::normalize;
use rayon::prelude::*;

const PAGE_RANK_ITERATIONS: u32 = 20;

/// Calculates and sets the PageRank for each Page
pub fn calculate_page_rank(pages: &mut [Page]) {
    for count in 0..PAGE_RANK_ITERATIONS {
        // Calculate the ranks
        let mut ranks: Vec<f64> = pages
            .par_iter()
            .map(|page| page_rank(page, pages))
            .collect();

        // Normalize the ranks on the final iteration
        if count == PAGE_RANK_ITERATIONS - 1 {
            normalize::high_is_better(&mut ranks);
        }

        // Assign the new ranks to the pages
        for (index, rank) in ranks.into_iter().enumerate() {
            pages[index].page_rank = rank;
        }
    }
}

fn page_rank(page: &Page, all_pages: &[Page]) -> f64 {
    let page_rank = all_pages.iter().fold(0.0, |page_rank, other_page| {
        if other_page.links.contains(&page.url) {
            page_rank + other_page.page_rank / other_page.links.len() as f64
        } else {
            page_rank
        }
    });

    0.15 + (page_rank * 0.85)
}
