use std::collections::HashSet;

#[derive(Debug, Serialize)]
pub struct Page {
    pub words: Vec<u32>,
    pub url: String,
    pub links: HashSet<String>,
    pub page_rank: f64,
}
