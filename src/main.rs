#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate tera;
#[macro_use]
extern crate gotham_derive;

use crate::extensions::AnyError;

mod engine;
mod extensions;
mod indexer;
mod model;
mod router;
mod routes;

fn main() -> Result<(), AnyError> {

    routes::init_engine();
    let addr = "127.0.0.1:8080";
    println!("Listening for requests at http://{}", addr);
    gotham::start(addr, router::router());

    Ok(())
}
