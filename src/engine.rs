use crate::indexer::Indexer;
use crate::model::entity::Page;
use crate::model::normalize;
use itertools::Itertools;
use rayon::prelude::*;

const PAGE_RANK_WEIGHT: f64 = 0.5;
const LOCATION_WEIGHT: f64 = 0.8;
const FREQUENCY_WEIGHT: f64 = 1.0;

#[derive(Debug)]
pub struct Engine {
    pages: Vec<Page>,
    indexer: Indexer,
}

#[derive(Debug, Serialize)]
pub struct QueryRanking<'a> {
    pub score: Score,
    pub page: &'a Page,
}

#[derive(Debug, Copy, Clone, Serialize)]
pub struct Score {
    pub page_rank: f64,
    pub frequency: f64,
    pub location: f64,
    pub total: f64,
}

impl Engine {
    pub fn new(pages: Vec<Page>, indexer: Indexer) -> Self {
        Engine { pages, indexer }
    }

    pub fn query(&self, query: &str) -> Vec<QueryRanking> {
        let query_ids = self.get_query_ids(query);
        if query_ids.is_empty() {
            return vec![];
        }

        let mut rankings: Vec<QueryRanking> = self
            .pages
            .par_iter()
            .map(|page| {
                let score = self.score_page(page, &query_ids);
                QueryRanking { score, page }
            })
            // Filter out pages that didn't have any of the query words
            .filter(|ranking| ranking.score.frequency > 0.0)
            .collect();

        if !rankings.is_empty() {
            normalize_and_weigh(&mut rankings);

            // Sort highest score first
            rankings.sort_unstable_by(|a, b| {
                a.score
                    .total
                    .partial_cmp(&b.score.total)
                    .unwrap() // blow up on NaN
                    .reverse()
            });
        }

        rankings
    }

    // Returns the id for a word or None if the word is not indexed
    fn get_query_ids(&self, query: &str) -> Vec<Option<u32>> {
        let queries = query.split_whitespace();
        queries
            .map(|q| self.indexer.get_id(&q.to_lowercase()))
            .collect()
    }
}

// Scoring
impl Engine {
    fn score_page(&self, page: &Page, query_ids: &[Option<u32>]) -> Score {
        let freq = self.score_frequency(page, query_ids);
        let loc = self.score_location(page, query_ids);

        Score {
            page_rank: page.page_rank,
            frequency: freq,
            location: loc,
            total: Default::default(), // is set when normalizing scores
        }
    }

    fn score_frequency(&self, page: &Page, query_ids: &[Option<u32>]) -> f64 {
        let valid = query_ids.iter().filter_map(|id| *id).collect_vec();

        page.words.iter().fold(0, |acc, page_word_id| {
            if valid.contains(page_word_id) {
                acc + 1
            } else {
                acc
            }
        }) as f64
    }

    fn score_location(&self, page: &Page, query_ids: &[Option<u32>]) -> f64 {
        let not_found_score = 100_000;

        query_ids
            .iter()
            .map(|id| {
                if let Some(id) = id {
                    page.words
                        .iter()
                        .enumerate()
                        .find(|index_id| index_id.1 == id)
                        .map(|index_id| index_id.0 + 1)
                        .unwrap_or(not_found_score)
                } else {
                    not_found_score
                }
            })
            .sum::<usize>() as f64
    }
}

struct AllScores {
    frequency: Vec<f64>,
    location: Vec<f64>,
}

// Normalize all scores so that they range from 0 (lowest) to 1 (highest).
// page rank is already normalized when it is calculated as this need to be done only once.
// After the scores are normalized, they are weighted according to the weight factors.
fn normalize_and_weigh(rankings: &mut [QueryRanking]) {
    let scores = AllScores {
        frequency: Vec::with_capacity(rankings.len()),
        location: Vec::with_capacity(rankings.len()),
    };

    let mut all = rankings.iter().fold(scores, |mut acc, ranking| {
        acc.frequency.push(ranking.score.frequency);
        acc.location.push(ranking.score.location);
        acc
    });

    normalize::high_is_better(&mut all.frequency);
    normalize::low_is_better(&mut all.location);

    for i in 0..rankings.len() {
        rankings[i].score.location = all.location[i];
        rankings[i].score.frequency = all.frequency[i];
        weigh(&mut rankings[i]);
    }
}

// Weigh the scores and set the total score
fn weigh(ranking: &mut QueryRanking) {
    ranking.score.page_rank *= PAGE_RANK_WEIGHT;
    ranking.score.location *= LOCATION_WEIGHT;
    ranking.score.frequency *= FREQUENCY_WEIGHT;
    ranking.score.total =
        ranking.score.page_rank + ranking.score.location + ranking.score.frequency;
}
